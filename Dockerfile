FROM openjdk:14
ADD target/pool-api.jar pool-api.jar
EXPOSE 8000
ENTRYPOINT ["java", "-jar", "pool-api.jar"]