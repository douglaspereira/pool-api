# pool-api

## Fluxo da votação de uma pauta pela API
- Url base: http://localhost:8000
 

- Cadastrar uma pauta:
  - POST /pools
    - payload: {String id, String subject}
    

- Iniciar uma sessão:
  - POST /pools/{id}/session
    - payload: {Int durationInSeconds}
    - (Ao criar uma sessão, é criado uma mensagem(via RabbitMq) agendada interna para avisar quando a sessão for finalizada)


- Votar:
  - POST /pools/{id}/vote
    - payload: {String partnerId, Boolean value}


- Verificar resultado da última sessão de uma Pauta:
  - GET /pools/{id}/result


## Tecnologias utilizadas
- Linguagem: Java
- Framework: Spring Boot
- Gerenciador de dependências: Maven
- Banco de dados: MongoDb
- Mensageria: RabbitMq
- Log: slf4j
- Testes de integração: Postman (json com a collection encontra-se pasta /resources)
- Testes unitários: JUnit & Mockito
- Contêiners: Docker 

## Ambiente necessário para execução
- Build da aplicação
- Banco mongodb:
  - https://www.mongodb.com/download-center/community
- RabbitMq instalado com o plugin delayed-messaging:
  - https://www.rabbitmq.com/download.html
  - https://github.com/rabbitmq/rabbitmq-delayed-message-exchange 
