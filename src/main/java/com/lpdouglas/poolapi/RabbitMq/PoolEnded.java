package com.lpdouglas.poolapi.RabbitMq;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class PoolEnded implements Serializable {
    private String poolId;
    private String sessionId;
}
