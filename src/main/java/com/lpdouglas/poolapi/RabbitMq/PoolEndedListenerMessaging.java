package com.lpdouglas.poolapi.RabbitMq;

import com.lpdouglas.poolapi.dto.SessionResultOutput;
import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.service.PoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PoolEndedListenerMessaging {

    private final PoolService poolService;
    private Logger logger = LoggerFactory.getLogger(PoolService.class);

    public PoolEndedListenerMessaging(PoolService poolService) {
        this.poolService = poolService;
    }

    public void receivePoolSessionHasEnded(PoolEnded poolEnded) {
        try {
        SessionResultOutput poolSessionResultBySessionId = poolService
                .getPoolSessionResultBySessionId(poolEnded.getPoolId(), poolEnded.getSessionId());
        System.out.printf("Result: %s%n", poolSessionResultBySessionId);
        } catch (BusinessException exception) {
            logger.info(String.format("%s, id: %s", exception.getMessage(), poolEnded.getPoolId()));
        }


    }

}
