package com.lpdouglas.poolapi.RabbitMq;

import com.lpdouglas.poolapi.config.RabbitmqConfiguration;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class PoolMessaging {

    private final RabbitTemplate rabbitTemplate;

    public PoolMessaging(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendPoolResultSession(String poolId, String sessionId, int durationInSeconds) {
        rabbitTemplate.convertAndSend(RabbitmqConfiguration.topicExchangeName,
                "session.result.end",
                new PoolEnded(poolId, sessionId),
                message -> {
                    message.getMessageProperties().setDelay(durationInSeconds * 1000);
                    return message;
                });
    }

}
