package com.lpdouglas.poolapi.config;

import com.lpdouglas.poolapi.RabbitMq.PoolEndedListenerMessaging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfiguration {

    public static final String topicExchangeName = "poolapi";
    public static final String queueName = "pool";
    Logger logger = LoggerFactory.getLogger(RabbitmqConfiguration.class);

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

//    @Bean
//    TopicExchange exchange() {
//        return new TopicExchange(topicExchangeName);
//    }

    @Bean
    Exchange exchange() {
        return ExchangeBuilder.topicExchange(topicExchangeName)
                .delayed()
                .build();
    }

//    @Bean
//    CustomExchange delayExchange() {
//        Map<String, Object> args = new HashMap<>();
//        args.put("x-delayed-type", "direct");
//        return new CustomExchange(topicExchangeName, "x-delayed-message", true, false, args);
//    }

    @Bean
    Binding binding(Queue queue, Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("session.result.#").noargs();
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(PoolEndedListenerMessaging receiver) {
        MessageListenerAdapter receivePoolSessionHasEnded = new MessageListenerAdapter(receiver, "receivePoolSessionHasEnded");
        logger.debug(String.format("Started new rabbitmq listener %s", receivePoolSessionHasEnded));
        return receivePoolSessionHasEnded;
    }


}
