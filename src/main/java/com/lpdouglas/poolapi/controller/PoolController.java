package com.lpdouglas.poolapi.controller;

import com.lpdouglas.poolapi.dto.PoolInput;
import com.lpdouglas.poolapi.dto.SessionResultOutput;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.service.PoolService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pools")
public class PoolController {

    final PoolService poolService;

    public PoolController(PoolService poolService) {
        this.poolService = poolService;
    }

    @PostMapping
    public Pool postPool(@Valid @RequestBody PoolInput pool)
    {
        return poolService.insertPool(pool);
    }

    @GetMapping("/{id}/result")
    public SessionResultOutput getPoolResult(@PathVariable String id){
        return poolService.getPoolSessionResult(id);
    }

    ///Others endpoints not required on test, just for api-test
    @GetMapping
    public List<Pool> getPools(){
        return poolService.getPools();
    }

    @GetMapping("/{id}")
    public Pool getPool(@PathVariable String id){
        return poolService.getPool(id);
    }

    @DeleteMapping("/{id}")
    public String deletePool(@PathVariable String id){
         poolService.deletePool(id);
         return "Ok";
    }

}
