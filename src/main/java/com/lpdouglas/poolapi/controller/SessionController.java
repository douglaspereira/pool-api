package com.lpdouglas.poolapi.controller;

import com.lpdouglas.poolapi.dto.SessionInput;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.service.SessionService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pools/{poolId}/sessions")
public class SessionController {

    private final SessionService sessionService;

    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @PostMapping
    public Session postSession(@PathVariable String poolId, @Valid @RequestBody SessionInput session) {
        return sessionService.insertSession(poolId, session);
    }

}
