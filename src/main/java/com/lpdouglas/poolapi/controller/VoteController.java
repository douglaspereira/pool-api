package com.lpdouglas.poolapi.controller;

import com.lpdouglas.poolapi.dto.VoteInput;
import com.lpdouglas.poolapi.model.Vote;
import com.lpdouglas.poolapi.service.VoteService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pools/{poolId}/vote")
public class VoteController {

    final VoteService voteService;

    public VoteController(VoteService voteService) {
        this.voteService = voteService;
    }


    @PostMapping
    public Vote postVote(@PathVariable String poolId, @Valid @RequestBody VoteInput vote)
    {
        return voteService.insertVote(poolId, vote);
    }
}
