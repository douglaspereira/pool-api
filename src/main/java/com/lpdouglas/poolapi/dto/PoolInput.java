package com.lpdouglas.poolapi.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

@Data
public class PoolInput {

    @Id
    private String id;

    @NotNull
    private String subject;

}
