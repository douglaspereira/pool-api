package com.lpdouglas.poolapi.dto;

import lombok.Data;


@Data
public class SessionInput {

    private int durationInSeconds = 60;

}
