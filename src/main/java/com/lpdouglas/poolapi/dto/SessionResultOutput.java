package com.lpdouglas.poolapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@Builder
@ToString
public class SessionResultOutput {
    private String poolSubject;
    private String poolId;
    private long yes;
    private long no;
    private Date createdAt;
    private Date expiredAt;
}
