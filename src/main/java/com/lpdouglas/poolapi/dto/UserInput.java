package com.lpdouglas.poolapi.dto;

import com.lpdouglas.poolapi.enumeration.UserInputEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInput {

    @NotNull
    private UserInputEnum status;

}
