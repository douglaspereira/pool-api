package com.lpdouglas.poolapi.dto;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class VoteInput {

    @NotNull
    @NotBlank
    @CPF
    private String partnerId;

    @NotNull
    private Boolean value;


}
