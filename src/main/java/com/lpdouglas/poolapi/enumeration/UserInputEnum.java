package com.lpdouglas.poolapi.enumeration;

public enum UserInputEnum {
    ABLE_TO_VOTE, UNABLE_TO_VOTE;

    public Boolean unableToVote(){
        return this.equals(UNABLE_TO_VOTE);
    }
}
