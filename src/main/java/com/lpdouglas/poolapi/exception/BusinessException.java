package com.lpdouglas.poolapi.exception;

import lombok.Data;


@Data
public class BusinessException extends RuntimeException {
    final String message;
}
