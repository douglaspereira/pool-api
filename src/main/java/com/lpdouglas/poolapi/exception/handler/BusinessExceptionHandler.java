package com.lpdouglas.poolapi.exception.handler;

import com.lpdouglas.poolapi.exception.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BusinessExceptionHandler {

    @ExceptionHandler
    protected ResponseEntity<String> handleBusinessException(BusinessException businessException){
        return new ResponseEntity<>(businessException.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
