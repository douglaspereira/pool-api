package com.lpdouglas.poolapi.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class InputValidationExceptionHandler {

    @ExceptionHandler
    protected ResponseEntity<List<String>> handleBindException(BindException exception) {
        return new ResponseEntity<>(getBindingResultErrors(exception.getBindingResult()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    protected ResponseEntity<List<String>> handleMethodArgumentNotValid(MethodArgumentNotValidException exception) {
        return new ResponseEntity<>(getBindingResultErrors(exception.getBindingResult()), HttpStatus.BAD_REQUEST);
    }

    private List<String> getBindingResultErrors(BindingResult bindingResult){
        final List<String> errors = new ArrayList<>();

        for (FieldError error : bindingResult.getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }

        for (ObjectError error : bindingResult.getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        return errors;
    }

}

