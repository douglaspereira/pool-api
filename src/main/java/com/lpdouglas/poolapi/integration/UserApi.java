package com.lpdouglas.poolapi.integration;

import com.lpdouglas.poolapi.dto.UserInput;
import com.lpdouglas.poolapi.model.Vote;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class UserApi {
    public UserInput getUserAbleToVote(Vote vote) {
        return WebClient.create()
                .get()
                .uri(String.format("https://user-info.herokuapp.com/users/%s", vote.getPartnerId()))
                .retrieve()
                .bodyToMono(UserInput.class)
                .block();
    }
}
