package com.lpdouglas.poolapi.mapper;

import com.lpdouglas.poolapi.dto.PoolInput;
import com.lpdouglas.poolapi.model.Pool;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class PoolMapper {

    public Pool inputToModel(PoolInput poolInput) {
        return new Pool(poolInput.getId(),
                poolInput.getSubject(),
                new ArrayList<>());
    }

}
