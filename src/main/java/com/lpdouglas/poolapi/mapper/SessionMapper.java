package com.lpdouglas.poolapi.mapper;

import com.lpdouglas.poolapi.dto.SessionInput;
import com.lpdouglas.poolapi.model.Session;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

@Component
public class SessionMapper {
    public Session inputToModel(SessionInput sessionInput) {
        Session session = new Session();
        session.setId(UUID.randomUUID().toString());
        session.setVotes(new ArrayList<>());

        Instant instant = Instant.now();
        session.setCreateAt(Date.from(instant));

        Instant instantPlusOneMinute = instant.plusSeconds(sessionInput.getDurationInSeconds());

        if (session.getExpiresIn() == null) {
            session.setExpiresIn(Date.from(instantPlusOneMinute));
        }

        return session;
    }
}
