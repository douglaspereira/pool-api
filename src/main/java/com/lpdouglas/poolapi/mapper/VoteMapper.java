package com.lpdouglas.poolapi.mapper;

import com.lpdouglas.poolapi.dto.VoteInput;
import com.lpdouglas.poolapi.model.Vote;
import org.springframework.stereotype.Component;

@Component
public class VoteMapper {
    public Vote inputToModel(VoteInput voteInput) {
        Vote vote = new Vote(voteInput.getPartnerId(), voteInput.getValue());
        return vote;
    }
}
