package com.lpdouglas.poolapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Document
public class Session {

    @Id
    private String id;
    private Date createAt;
    private Date expiresIn;
    private List<Vote> votes;
}
