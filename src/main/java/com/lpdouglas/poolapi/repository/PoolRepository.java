package com.lpdouglas.poolapi.repository;

import com.lpdouglas.poolapi.model.Pool;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PoolRepository extends MongoRepository<Pool, String> {
}
