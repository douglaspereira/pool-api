package com.lpdouglas.poolapi.service;

import com.lpdouglas.poolapi.dto.PoolInput;
import com.lpdouglas.poolapi.dto.SessionResultOutput;
import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.mapper.PoolMapper;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.model.Vote;
import com.lpdouglas.poolapi.repository.PoolRepository;
import com.lpdouglas.poolapi.validation.PoolValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PoolService {

    private final PoolRepository poolRepository;
    private final PoolMapper poolMapper;
    private final Logger logger;

    public PoolService(PoolRepository poolRepository, PoolMapper poolMapper) {
        this.poolRepository = poolRepository;
        this.poolMapper = poolMapper;
        this.logger = LoggerFactory.getLogger(PoolService.class);
    }

    public List<Pool> getPools() {
        return poolRepository.findAll();
    }

    public Pool getPool(String id) {

        PoolValidation.id(id);

        Optional<Pool> optionalPool = poolRepository.findById(id);
        PoolValidation.validate(optionalPool);

        //noinspection OptionalGetWithoutIsPresent
        return optionalPool.get();
    }

    public Pool insertPool(PoolInput poolInput) {
        Optional<Pool> poolOld = poolRepository.findById(poolInput.getId());

        PoolValidation.insert(poolOld);

        Pool pool = poolMapper.inputToModel(poolInput);


        Pool savedPool = poolRepository.save(pool);
        logger.info(String.format("Created Pool '%s'", savedPool));

        return savedPool;
    }

    public void deletePool(String id) {
        poolRepository.deleteById(id);
        logger.debug(String.format("Deleted Pool '%s'", id));
    }


    public SessionResultOutput getPoolSessionResult(String poolId) {
        Pool pool = getPool(poolId);
        Session session = getLastSessionEnded(pool);

        return createSessionResultOutput(pool, session);
    }


    public SessionResultOutput getPoolSessionResultBySessionId(String poolId, String sessionId) {
        Pool pool = getPool(poolId);
        List<Session> sessions = pool.getSessions();

        for (Session session :
                sessions) {
            if (session.getId().contentEquals(sessionId)) {
                return createSessionResultOutput(pool, session);
            }
        }

        throw new BusinessException("session does not exist");
    }

    private SessionResultOutput createSessionResultOutput(Pool pool, Session session) {
        List<Vote> votes = session.getVotes();
        long yesVotes = votes.stream().filter(Vote::getValue).count();
        long noVotes = votes.size() - yesVotes;

        return SessionResultOutput.builder()
                .poolId(pool.getId())
                .poolSubject(pool.getSubject())
                .yes(yesVotes)
                .no(noVotes)
                .createdAt(session.getCreateAt())
                .expiredAt(session.getExpiresIn())
                .build();
    }


    private Session getLastSessionEnded(Pool pool) {
        List<Session> sessions = pool.getSessions();

        Session lastSession = null;
        Date dateNow = new Date();

        for (Session session :
                sessions) {
            if (session.getExpiresIn().before(dateNow)) {
                lastSession = session;
            }
        }

        if (lastSession == null) {
            throw new BusinessException("No sessions ended");
        }

        return lastSession;
    }


}
