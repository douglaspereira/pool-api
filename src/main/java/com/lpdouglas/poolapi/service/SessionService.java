package com.lpdouglas.poolapi.service;

import com.lpdouglas.poolapi.RabbitMq.PoolMessaging;
import com.lpdouglas.poolapi.dto.SessionInput;
import com.lpdouglas.poolapi.mapper.SessionMapper;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.repository.PoolRepository;
import com.lpdouglas.poolapi.validation.SessionValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessionService {

    private final PoolRepository poolRepository;
    private final PoolService poolService;
    private final PoolMessaging poolMessaging;
    private final SessionMapper sessionMapper;
    private final Logger logger;

    public SessionService(PoolRepository poolRepository, PoolService poolService, PoolMessaging poolMessaging, SessionMapper sessionMapper) {
        this.poolRepository = poolRepository;
        this.poolService = poolService;
        this.poolMessaging = poolMessaging;
        this.sessionMapper = sessionMapper;
        this.logger = LoggerFactory.getLogger(SessionService.class);
    }

    public Session insertSession(String poolId, SessionInput sessionInput) {
        Pool pool = poolService.getPool(poolId);
        List<Session> sessions = pool.getSessions();

        SessionValidation.validate(sessions);

        Session session = sessionMapper.inputToModel(sessionInput);
        sessions.add(session);

        poolMessaging.sendPoolResultSession(poolId, session.getId(), sessionInput.getDurationInSeconds());
        poolRepository.save(pool);
        logger.info(String.format("Started new Session( %s ) on Pool '%s'", session, pool.getId()));
        return session;
    }


}
