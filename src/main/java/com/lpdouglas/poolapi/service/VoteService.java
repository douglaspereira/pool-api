package com.lpdouglas.poolapi.service;

import com.lpdouglas.poolapi.dto.UserInput;
import com.lpdouglas.poolapi.dto.VoteInput;
import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.integration.UserApi;
import com.lpdouglas.poolapi.mapper.VoteMapper;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.model.Vote;
import com.lpdouglas.poolapi.repository.PoolRepository;
import com.lpdouglas.poolapi.validation.VoteValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class VoteService {
    private final PoolRepository poolRepository;
    private final PoolService poolService;
    private final VoteMapper voteMapper;
    private final Logger logger;
    private final UserApi userApi;


    public VoteService(PoolRepository poolRepository, PoolService poolService, VoteMapper voteMapper, UserApi userApi) {
        this.poolRepository = poolRepository;
        this.poolService = poolService;
        this.voteMapper = voteMapper;
        this.logger = LoggerFactory.getLogger(VoteService.class);
        this.userApi = userApi;
    }

    public Vote insertVote(String id, VoteInput voteInput) {
        Pool pool = poolService.getPool(id);

        VoteValidation.validate(pool);

        Vote vote = voteMapper.inputToModel(voteInput);
        Session openedSession = getOpenedSession(pool);
        List<Vote> votes = openedSession.getVotes();

        VoteValidation.haventVote(vote.getPartnerId(), votes);

        //API INTEGRATION JUST ON FINAL STEP TO BETTER SPEED ON PROCESS INVALID VOTES
        UserInput user = userApi.getUserAbleToVote(vote);
        logger.debug(String.format("userVoteApi response: %s", vote));
        VoteValidation.ableToVote(user);

        votes.add(vote);
        poolRepository.save(pool);
        logger.info(String.format("New vote on pool '%s': %s", pool.getId(), vote));
        return vote;
    }



    private Session getOpenedSession(Pool pool) {
        List<Session> sessions = pool.getSessions();
        for (Session session: sessions) {
            if (session.getExpiresIn().after(new Date())) {
                return session;
            }
        }

        throw new BusinessException("Pool has no active session");
    }

}
