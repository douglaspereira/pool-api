package com.lpdouglas.poolapi.validation;

import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.model.Pool;

import java.util.Optional;

public class PoolValidation {
    public static void validate(Optional<Pool> pool){
        if (pool.isEmpty()) {
            throw new BusinessException("pool does not exists");
        }
    }

    public static void id(String id) {
        if (id == null || id.isBlank()) {
            throw new BusinessException("invalid pool id");
        }
    }

    public static void insert(Optional<Pool> poolOld) {
        if (poolOld.isPresent()) {
            throw new BusinessException(String.format("Id '%s' already exists", poolOld.get().getId()));
        }
    }
}
