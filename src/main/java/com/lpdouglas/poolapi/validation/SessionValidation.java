package com.lpdouglas.poolapi.validation;

import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.model.Session;

import java.util.Date;
import java.util.List;

public class SessionValidation {
    public static void validate(List<Session> sessions) {
        Date dateNow = new Date();
        for (Session session :
                sessions) {
            if (session.getExpiresIn().after(dateNow)) {
                throw new BusinessException("Cannot start a new session: Session " + session.getId() + " is open.");
            }
        }
    }
}
