package com.lpdouglas.poolapi.validation;

import com.lpdouglas.poolapi.dto.UserInput;
import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.model.Vote;

import java.util.List;

public class VoteValidation {

    public static void validate(Pool pool) {
        List<Session> sessions = pool.getSessions();
        if (sessions.size() == 0){
            throw new BusinessException("no session on this pool");
        }
    }

    public static void haventVote(String partnerId, List<Vote> votes) {
        for (Vote vote : votes) {
            if (vote.getPartnerId().contentEquals(partnerId)) {
                throw new BusinessException("Partner already vote on this session");
            }
        }
    }

    public static void ableToVote(UserInput user) {
        if (user == null || user.getStatus().unableToVote()) {
            throw new BusinessException("Partner unable to vote");
        }
    }
}
