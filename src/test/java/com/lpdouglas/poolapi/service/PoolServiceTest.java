package com.lpdouglas.poolapi.service;

import com.lpdouglas.poolapi.dto.PoolInput;
import com.lpdouglas.poolapi.dto.SessionInput;
import com.lpdouglas.poolapi.dto.SessionResultOutput;
import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.mapper.PoolMapper;
import com.lpdouglas.poolapi.mapper.SessionMapper;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.repository.PoolRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
public class PoolServiceTest {

    @InjectMocks
    PoolService poolService;

    @Mock
    PoolRepository poolRepository;

    @Mock
    PoolMapper poolMapper;

    String validId = "validId";
    String invalidId = "invalidId";
    String validSessionId = "validSessionId";
    SessionMapper sessionMapper;
    SessionInput sessionInput;
    Session session;

    @Before
    public void init() {
        sessionMapper = new SessionMapper();
        sessionInput = new SessionInput();
        sessionInput.setDurationInSeconds(-50);
        session = sessionMapper.inputToModel(sessionInput);
        session.setId(validSessionId);

        when(poolRepository.findById(validId))
                .thenReturn(Optional.of(new Pool(validId,
                        "Test Pool",
                        List.of(session))));

        when(poolRepository.findAll())
                .thenReturn(
                        List.of(
                            new Pool(validId, "Test Pool", new ArrayList<>()),
                            new Pool("anotherId", "EatPool", new ArrayList<>())
                        )
                );

        when(poolRepository.save(any(Pool.class))).thenAnswer(
                arguments -> {
                    Pool pool = arguments.getArgument(0);
                    String poolId = (pool.getId() != null) ? pool.getId() : UUID.randomUUID().toString();

                    return new Pool(poolId, pool.getSubject(), new ArrayList<>());
                }
        );

        when(poolMapper.inputToModel(any())).thenCallRealMethod();
    }

    @Test
    public void getPools() {
        List<Pool> pools = poolService.getPools();

        verify(poolRepository, times(1)).findAll();
        assert pools.size() == 2;
    }

    @Test
    public void getPool() {
        Pool pool = poolService.getPool(validId);

        verify(poolRepository, times(1)).findById(anyString());
        assert pool.getId().contentEquals(validId);
        assert pool.getSubject().contentEquals("Test Pool");
        assert pool.getSessions() != null;
    }

    @Test(expected = BusinessException.class)
    public void getPoolInvalidId() {
        Pool pool = poolService.getPool("invalidId");

        verify(poolRepository, times(1)).findById(anyString());
        assert pool.getId().contentEquals(validId);
        assert pool.getSubject().contentEquals("Test Pool");
        assert pool.getSessions() != null;
    }

    @Test
    public void insertPool() {
        PoolInput poolInput = new PoolInput();
        String subject = "My Pool";
        poolInput.setSubject(subject);
        Pool pool = poolService.insertPool(poolInput);

        verify(poolRepository, times(1)).save(any());
        assert pool.getSubject().contentEquals(subject);
        assert pool.getId() != null && !pool.getId().isBlank();
    }

    @Test(expected = BusinessException.class)
    public void insertPoolIdDuplicated() {
        PoolInput poolInput = new PoolInput();
        poolInput.setId(validId);

        poolService.insertPool(poolInput);
    }

    @Test
    public void deletePool() {
        poolService.deletePool(validId);

        verify(poolRepository, times(1)).deleteById(any());
    }

    @Test
    public void getPoolSessionResult() {
        SessionResultOutput poolSessionResult = poolService.getPoolSessionResult(validId);

        verify(poolRepository).findById(anyString());
        assert  poolSessionResult.getExpiredAt() != null;
    }

    @Test(expected = BusinessException.class)
    public void getPoolSessionResultInvalidId() {
        poolService.getPoolSessionResult(invalidId);
    }

    @Test
    public void getPoolSessionResultBySessionId() {
        SessionResultOutput poolSessionResult = poolService.getPoolSessionResultBySessionId(validId, validSessionId);

        verify(poolRepository).findById(anyString());
        assert poolSessionResult.getPoolId().contentEquals(validId);
        assert session.getCreateAt().equals(poolSessionResult.getCreatedAt());
    }

    @Test(expected = BusinessException.class)
    public void getPoolSessionResultBySessionInvalidId() {
        poolService.getPoolSessionResultBySessionId(validId, invalidId);
    }

}