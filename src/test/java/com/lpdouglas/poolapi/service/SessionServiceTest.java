package com.lpdouglas.poolapi.service;

import com.lpdouglas.poolapi.RabbitMq.PoolMessaging;
import com.lpdouglas.poolapi.dto.SessionInput;
import com.lpdouglas.poolapi.mapper.SessionMapper;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.repository.PoolRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class SessionServiceTest {

    @InjectMocks
    SessionService sessionService;

    @Mock
    PoolService poolService;

    @Mock
    PoolRepository poolRepository;

    @Mock
    SessionMapper sessionMapper;

    @Mock
    PoolMessaging poolMessaging;

    String validId = "validId";
    SessionInput sessionInput;


    @Before
    public void init() {

        Pool pool = new Pool(validId, "Test Pool", new ArrayList<>());
        when(poolService.getPool(anyString())).thenReturn(pool);

        doNothing().when(poolMessaging).sendPoolResultSession(anyString(), anyString(), anyInt());
        when(sessionMapper.inputToModel(any())).thenCallRealMethod();
    }

    @Test
    public void insertSession() {
        sessionInput = new SessionInput();
        sessionInput.setDurationInSeconds(60);

        sessionService.insertSession(validId, sessionInput);

        verify(poolService, times(1)).getPool(anyString());
        verify(poolRepository, times(1)).save(any());
        verify(poolMessaging, times(1)).sendPoolResultSession(anyString(), anyString(), anyInt());
        verify(sessionMapper, times(1)).inputToModel(any());
    }
}