package com.lpdouglas.poolapi.service;

import com.lpdouglas.poolapi.dto.SessionInput;
import com.lpdouglas.poolapi.dto.UserInput;
import com.lpdouglas.poolapi.dto.VoteInput;
import com.lpdouglas.poolapi.enumeration.UserInputEnum;
import com.lpdouglas.poolapi.exception.BusinessException;
import com.lpdouglas.poolapi.integration.UserApi;
import com.lpdouglas.poolapi.mapper.SessionMapper;
import com.lpdouglas.poolapi.mapper.VoteMapper;
import com.lpdouglas.poolapi.model.Pool;
import com.lpdouglas.poolapi.model.Session;
import com.lpdouglas.poolapi.repository.PoolRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class VoteServiceTest {

    @InjectMocks
    VoteService voteService;
    @Mock
    PoolRepository poolRepository;
    @Mock
    PoolService poolService;
    @Mock
    VoteMapper voteMapper;
    @Mock
    UserApi userApi;
    String validId = "validId";
    VoteInput voteInput;

    @Before
    public void init() {
        SessionMapper sessionMapper = new SessionMapper();
        SessionInput sessionInput = new SessionInput();
        sessionInput.setDurationInSeconds(180);
        Session session = sessionMapper.inputToModel(sessionInput);
        Pool pool = new Pool(validId, "Test Pool", List.of(session));
        voteInput = new VoteInput();
        voteInput.setPartnerId("partner");
        voteInput.setValue(true);

        when(poolService.getPool(anyString())).thenReturn(pool);
        when(poolService.getPool(anyString())).thenReturn(pool);
        when(voteMapper.inputToModel(any())).thenCallRealMethod();
    }

    @Test
    public void insertVote(){
        when(userApi.getUserAbleToVote(any())).thenReturn(new UserInput(UserInputEnum.ABLE_TO_VOTE));

        voteService.insertVote(validId, voteInput);

        verify(poolService, times(1)).getPool(anyString());
    }

    @Test(expected = BusinessException.class)
    public void insertVoteUnableToVote(){
        when(userApi.getUserAbleToVote(any())).thenReturn(new UserInput(UserInputEnum.UNABLE_TO_VOTE));

        voteService.insertVote(validId, voteInput);
    }
}